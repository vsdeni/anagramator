package com.vsdeni.anagramator.presentation;

import android.widget.EditText;

import com.vsdeni.anagramator.model.Word;

import java.util.List;

/**
 * Created by Deni on 19.04.2016.
 */
public interface WordsView {
    void onSearch(EditText letters);

    void showWords(List<Word> words);

    void showProgressbar(boolean show);
}
