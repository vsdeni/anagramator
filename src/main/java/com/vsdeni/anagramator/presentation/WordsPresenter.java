package com.vsdeni.anagramator.presentation;

import android.widget.EditText;

import com.vsdeni.anagramator.model.Word;
import com.vsdeni.anagramator.model.interactors.WordsInteractor;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Deni on 19.04.2016.
 */
public class WordsPresenter implements WordsInteractor.OnSearchFinishedListener, WordsInteractor.OnLoadFinishedListener {
    WordsView mView;

    WordsInteractor mInteractor;

    @Inject
    @Singleton
    public WordsPresenter(WordsInteractor interactor) {
        mInteractor = interactor;
    }

    public void setView(WordsView view) {
        mView = view;
        if (mView != null) {
            refresh();
        }
    }

    public void refresh() {
        mView.showProgressbar(true);
        mInteractor.loadWords(this);
    }

    public void onSearchWords(EditText editText) {
        mView.showProgressbar(true);
        String letters = editText.getText().toString();
        mInteractor.findWords(letters, this);
    }

    @Override
    public void onSearchFinished(List<Word> words) {
        mView.showWords(words);
        mView.showProgressbar(false);
    }

    @Override
    public void onLoadFinished() {
        mView.showProgressbar(false);
    }
}
