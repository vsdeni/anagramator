package com.vsdeni.anagramator.ui.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.vsdeni.anagramator.App;
import com.vsdeni.anagramator.R;
import com.vsdeni.anagramator.model.Word;
import com.vsdeni.anagramator.presentation.WordsPresenter;
import com.vsdeni.anagramator.presentation.WordsView;
import com.vsdeni.anagramator.ui.adapters.WordsAdapter;
import com.vsdeni.anagramator.ui.widget.DividerItemDecoration;
import com.vsdeni.anagramator.ui.widget.Keyboard;
import com.vsdeni.anagramator.ui.widget.TileEditText;
import com.vsdeni.anagramator.utils.Utils;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements WordsView, Keyboard.KeyboardListener {
    @Bind(R.id.letters)
    TileEditText mEditText;

    @Bind(R.id.list)
    RecyclerView mRecyclerView;

    @Bind(R.id.progress)
    ProgressBar mProgressBar;

    @Bind(R.id.letters_container)
    FrameLayout mInputView;

    @Bind(R.id.keyboard)
    Keyboard mKeyboard;

    WordsAdapter mWordsAdapter;

    @Inject
    WordsPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mWordsAdapter = new WordsAdapter(null);
        mRecyclerView.setAdapter(mWordsAdapter);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST));

        ViewCompat.setElevation(mRecyclerView, Utils.convertDpToPixel(3, this));
        ViewCompat.setElevation(mInputView, Utils.convertDpToPixel(4, this));

        App.getAppComponent(this).inject(this);
        mPresenter.setView(this);
        mKeyboard.setEditText(mEditText);
        mKeyboard.setKeyboardListener(this);

        mEditText.setFloatingLabelText(getString(R.string.letters_hint, 0));

        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mWordsAdapter.setData(null);
                mWordsAdapter.setNotFound(false);
                mWordsAdapter.notifyDataSetChanged();
                mEditText.setFloatingLabelText(getString(R.string.letters_hint, mEditText.getText().toString().length()));
            }
        });

        mEditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.onTouchEvent(event);
                setKeyboardVisibility(true);

                InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);  // hide the soft keyboard
                }

                return true;
            }
        });
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        onOkPressed();
    }

    private void setKeyboardVisibility(boolean visibility) {
        mKeyboard.setVisibility(visibility ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onBackPressed() {
        if (mKeyboard.getVisibility() == View.VISIBLE) {
            setKeyboardVisibility(false);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onSearch(EditText editText) {
        mPresenter.onSearchWords(editText);
        setKeyboardVisibility(false);
    }

    @Override
    public void showWords(List<Word> words) {
        if (words == null || words.size() == 0) {
            mWordsAdapter.setNotFound(true);
        }
        mWordsAdapter.setData(words);
        mWordsAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.setView(null);
        mKeyboard.setKeyboardListener(null);
    }

    @Override
    public void showProgressbar(boolean show) {
        mProgressBar.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void onOkPressed() {
        onSearch(mEditText);
    }
}