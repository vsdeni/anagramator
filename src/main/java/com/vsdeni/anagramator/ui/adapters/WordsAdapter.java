package com.vsdeni.anagramator.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vsdeni.anagramator.App;
import com.vsdeni.anagramator.R;
import com.vsdeni.anagramator.model.Word;

import java.util.List;

import butterknife.Bind;

/**
 * Created by Deni on 19.04.2016.
 */
public class WordsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    final static int TYPE_EMPTY = 0;
    final static int TYPE_WORD = 1;
    final static int TYPE_NOT_FOUND = 2;

    List<Word> mDataset;

    boolean mNotFound;

    public void setNotFound(boolean notFound) {
        mNotFound = notFound;
    }

    public static class WordsViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.text)
        public TextView text;

        public WordsViewHolder(View v) {
            super(v);
            text = (TextView) v.findViewById(R.id.text);
        }
    }

    public static class HeaderViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.text)
        public TextView text;

        public HeaderViewHolder(View v) {
            super(v);
            text = (TextView) v.findViewById(R.id.text);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mDataset == null || mDataset.size() == 0) {
            if (mNotFound) {
                return TYPE_NOT_FOUND;
            } else {
                return TYPE_EMPTY;
            }
        } else {
            return TYPE_WORD;
        }
    }

    public WordsAdapter(List<Word> data) {
        mDataset = data;
    }

    public void setData(List<Word> data) {
        mDataset = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        switch (viewType) {
            case TYPE_EMPTY:
            case TYPE_NOT_FOUND:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty, parent, false);
                return new HeaderViewHolder(v);
            default:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_word, parent, false);
                return new WordsViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case TYPE_WORD:
                WordsViewHolder wordsViewHolder = (WordsViewHolder) holder;
                Word word = mDataset.get(position);
                wordsViewHolder.text.setText(word.getValue());
                break;
            case TYPE_EMPTY:
                HeaderViewHolder emptyListViewHolder = (HeaderViewHolder) holder;
                emptyListViewHolder.text.setText(R.string.empty_list_header);
                emptyListViewHolder.text.setTextColor(App.getInstance().getResources().getColor(R.color.gray));
                break;
            case TYPE_NOT_FOUND:
                HeaderViewHolder notFoundViewHolder = (HeaderViewHolder) holder;
                notFoundViewHolder.text.setText(R.string.words_not_found);
                notFoundViewHolder.text.setTextColor(App.getInstance().getResources().getColor(R.color.red));
                break;
        }
    }

    @Override
    public int getItemCount() {
        if (mDataset == null || mDataset.size() == 0) {
            //to show header item
            return 1;
        }
        return mDataset.size();
    }
}
