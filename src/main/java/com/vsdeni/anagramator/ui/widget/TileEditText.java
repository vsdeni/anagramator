package com.vsdeni.anagramator.ui.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.Layout;
import android.text.Selection;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.vsdeni.anagramator.R;
import com.vsdeni.anagramator.utils.Utils;

/**
 * Created by Deni on 13-May-16.
 */
public class TileEditText extends MaterialEditText {
    /**
     * How much tiles can be in 1 line of EditText
     */
    int mCountPerLine;

    /**
     * Every tile is square so only 1 size required
     */
    int mTileSize;

    /**
     * Margin for every tile
     */
    int mTileMargin;

    /**
     * Size of letter inside tile
     */
    int mTextSize;

    /**
     * Paint of letter inside tile
     */
    Paint mTextPaint;

    /**
     * Bitmap which drown on every tile (for stroke effect)
     */
    Bitmap mTileBitmap;

    /**
     * Tile bg color if letter is known
     */
    int mTileColorKnownLetter;

    /**
     * Tile bg color if letter is unknown
     */
    int mTileColorUnknownLetter;

    /**
     * Symbol of unknown letter
     */
    private static final String UNKNOWN_LETTER_SYMBOL = "?";

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        int width = right - left;
        mCountPerLine = getContext().getResources().getInteger(R.integer.tile_edit_text_count_per_line);
        mTileSize = (int) ((float) (width - (mTileMargin * (mCountPerLine - 1))) / (float) mCountPerLine);
        mTextSize = mTileSize / 2;

        mTileBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.unexact_bg);
        mTileBitmap = Bitmap.createScaledBitmap(mTileBitmap, mTileSize, mTileSize, true);

        mTextPaint = new Paint();
        mTextPaint.setTextSize(mTextSize);
        mTextPaint.setColor(Color.BLACK);
        mTextPaint.setFakeBoldText(true);
        mTextPaint.setTextAlign(Paint.Align.CENTER);

        setMinimumHeight(mTileSize + (mTileMargin * 2) + getPaddingBottom() + getPaddingTop());
    }

    @Override
    public void append(CharSequence text, int start, int end) {
        getText().replace(Math.min(start, end), Math.max(start, end),
                text, 0, text.length());
        if (end == 0) {
            setSelection(getText().length());
        }
        refresh();
    }

    public void delete(int start, int end) {
        start = Math.min(start, end - 1);
        getText().delete(start, end);
        refresh();
    }

    public TileEditText(Context context) {
        super(context);
        init();
    }

    public TileEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TileEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    public boolean isSuggestionsEnabled() {
        return false;
    }

    private void init() {

        if (isInEditMode()) {
            return;
        }

        mTileMargin = (int) Utils.convertDpToPixel(3, getContext());

        setMovementMethod(LongClickLinkMovementMethod.getInstance());
        setLongClickable(false);
        mTileColorKnownLetter = getResources().getColor(R.color.colorKnownLetter);
        mTileColorUnknownLetter = getResources().getColor(R.color.colorUnknownLetter);
        setLongClickable(false);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    public BitmapDrawable decorateLetter(String letter, int index, int type) {
        Bitmap bitmap = Bitmap.createBitmap(mTileSize, mTileSize, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);

        int bgColor;
        if (UNKNOWN_LETTER_SYMBOL.equalsIgnoreCase(letter)) {
            bgColor = mTileColorUnknownLetter;
        } else {
            bgColor = mTileColorKnownLetter;
        }

        canvas.drawColor(bgColor);
        canvas.drawBitmap(mTileBitmap, 0, 0, null);

        int xPos = (canvas.getWidth() / 2);
        int yPos = (int) ((canvas.getHeight() / 2) - ((mTextPaint.descent() + mTextPaint.ascent()) / 2));
        canvas.drawText(letter, xPos, yPos, mTextPaint);

        return new BitmapDrawable(getResources(), bitmap);
    }

    public void refresh() {
        SpannableString spannableString = new SpannableString(getText().toString());
        Editable editable = getText();
        int count = editable.length();
        for (int i = 0; i < count; i++) {
            Drawable d = decorateLetter(editable.toString().substring(i, i + 1), i, 0);

            int left;
            int right;
            int top = mTileMargin;
            int bottom = d.getIntrinsicHeight() + mTileMargin;
            if (i % mCountPerLine == 0) {
                //first symbol without left margin
                left = 0;
                right = d.getIntrinsicWidth();
            } else {
                left = mTileMargin;
                right = d.getIntrinsicWidth() + mTileMargin;
            }

            d.setBounds(left, top, right, bottom);

            ImageSpan span = new ImageSpan(d, ImageSpan.ALIGN_BASELINE);
            spannableString.setSpan(span, i, i + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            int selected = getSelectionEnd();
            setText(spannableString);
            setSelection(selected);
        }
    }

    public class SpecialClickableSpan extends ClickableSpan {

        Integer mIndex;

        public SpecialClickableSpan(int index) {
            super();
            mIndex = index;
        }

        public void onLongClick(View widget) {
            refresh();
        }

        @Override
        public void onClick(View widget) {

        }
    }

    public static class LongClickLinkMovementMethod extends LinkMovementMethod {

        private Long lastClickTime = 0l;
        private int lastX = 0;
        private int lastY = 0;

        @Override
        public boolean onTouchEvent(TextView widget, Spannable buffer,
                                    MotionEvent event) {
            int action = event.getAction();

            if (action == MotionEvent.ACTION_UP ||
                    action == MotionEvent.ACTION_DOWN) {
                int x = (int) event.getX();
                int y = (int) event.getY();
                lastX = x;
                lastY = y;
                int deltaX = Math.abs(x - lastX);
                int deltaY = Math.abs(y - lastY);

                x -= widget.getTotalPaddingLeft();
                y -= widget.getTotalPaddingTop();

                x += widget.getScrollX();
                y += widget.getScrollY();

                Layout layout = widget.getLayout();
                int line = layout.getLineForVertical(y);
                int off = layout.getOffsetForHorizontal(line, x);

                SpecialClickableSpan[] link = buffer.getSpans(off, off, SpecialClickableSpan.class);

                if (link.length != 0) {
                    if (action == MotionEvent.ACTION_UP) {
                        if (System.currentTimeMillis() - lastClickTime < 500)
                            link[0].onClick(widget);
                        else if (deltaX < 10 && deltaY < 10)
                            link[0].onLongClick(widget);
                    } else if (action == MotionEvent.ACTION_DOWN) {
                        Selection.setSelection(buffer,
                                buffer.getSpanStart(link[0]),
                                buffer.getSpanEnd(link[0]));
                        lastClickTime = System.currentTimeMillis();
                    }
                    return true;
                }
            }

            return super.onTouchEvent(widget, buffer, event);
        }


        public static MovementMethod getInstance() {
            if (sInstance == null)
                sInstance = new LongClickLinkMovementMethod();

            return sInstance;
        }

        private static LongClickLinkMovementMethod sInstance;
    }
}
