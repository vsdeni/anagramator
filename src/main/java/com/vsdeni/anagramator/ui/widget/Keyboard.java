package com.vsdeni.anagramator.ui.widget;

import android.content.Context;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.ImageButton;

import com.vsdeni.anagramator.R;

/**
 * Created by Deni on 14-May-16.
 */
public class Keyboard extends FrameLayout implements View.OnClickListener {
    TileEditText mEditText;
    KeyboardListener mListener;

    public void setKeyboardListener(KeyboardListener listener) {
        mListener = listener;
    }

    public Keyboard(Context context) {
        super(context);
        init();
    }

    public Keyboard(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Keyboard(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.keyboard, this, true);
        initButtons(this);
    }

    private void initButtons(ViewGroup viewGroup) {
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = viewGroup.getChildAt(i);
            if (child instanceof Button || child instanceof ImageButton) {
                child.setOnClickListener(this);
            } else if (child instanceof ViewGroup) {
                initButtons((ViewGroup) child);
            }
        }
    }

    public void setEditText(TileEditText editText) {
        mEditText = editText;
    }

    private void vibrate() {
        Vibrator v = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(10);
    }

    @Override
    public void onClick(View v) {
        String value = (String) v.getTag();
        int start = Math.max(mEditText.getSelectionStart(), 0);
        int end = Math.max(mEditText.getSelectionEnd(), 0);
        if (value != null) {
            mEditText.append(value, start, end);
        } else if (v.getId() == R.id.keyBackspace && end != 0) {
            mEditText.delete(start, end);
        } else if (v.getId() == R.id.keyOk) {
            if (mListener != null) {
                mListener.onOkPressed();
            }
        }
        vibrate();
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        int width = right - left;
        GridLayout gridLayout = (GridLayout) getChildAt(0);
        int columnCount = gridLayout.getColumnCount();
        int columnWidht = (int) ((float) width / (float) columnCount);
        int childCount = gridLayout.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View view = gridLayout.getChildAt(i);
            GridLayout.LayoutParams params = (GridLayout.LayoutParams) view.getLayoutParams();
            if (view.getId() == R.id.keyUnknown) {
                params.width = columnWidht * 2;
            } else if (view.getId() == R.id.keyHint) {
                params.width = columnWidht * 7;
            } else if (view.getId() == R.id.keyOk) {
                params.width = columnWidht * 2;
            } else {
                params.width = columnWidht;
            }
        }
    }

    public interface KeyboardListener {
        void onOkPressed();
    }
}
