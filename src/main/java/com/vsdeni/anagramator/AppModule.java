package com.vsdeni.anagramator;

import android.app.Application;

import com.vsdeni.anagramator.model.interactors.DatabaseWordsInteractor;
import com.vsdeni.anagramator.model.interactors.WordsInteractor;
import com.vsdeni.anagramator.presentation.WordsPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Deni on 12-May-16.
 */
@Module
public class AppModule {
    Application mApp;

    public AppModule(App app) {
        mApp = app;
    }

    @Provides
    WordsInteractor provideInteractor() {
        return new DatabaseWordsInteractor();
    }

    @Provides
    WordsPresenter providePresenter(WordsInteractor wordsInteractor) {
        return new WordsPresenter(wordsInteractor);
    }
}
