package com.vsdeni.anagramator.model;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.Required;

/**
 * Created by Deni on 19.04.2016.
 */
public class Word extends RealmObject {
    @Index
    @Required
    private String value;

    @Index
    private int size;

    public Word(String value) {
        this.value = value;
        this.size = value.length();
    }

    public Word() {
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
        this.size = value.length();
    }

    public int getSize() {
        return size;
    }
}