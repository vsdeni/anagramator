package com.vsdeni.anagramator.model.interactors;

import android.content.Context;
import android.os.AsyncTask;

import com.vsdeni.anagramator.App;
import com.vsdeni.anagramator.R;
import com.vsdeni.anagramator.model.Word;
import com.vsdeni.anagramator.utils.AssetsTextReaderHelper;
import com.vsdeni.anagramator.utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import timber.log.Timber;

/**
 * Created by Deni on 19.04.2016.
 */
public class DatabaseWordsInteractor implements WordsInteractor {
    Character mUnknownSymbol;
    Context mContext;

    public DatabaseWordsInteractor() {
        mContext = App.getInstance();
        mUnknownSymbol = mContext.getString(R.string.keyboard_key_unknown).charAt(0);
    }

    @Override
    public void loadWords(final OnLoadFinishedListener listener) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                Realm realm = Realm.getDefaultInstance();
                if (realm.where(Word.class).count() == 0) {
                    List<Word> words = AssetsTextReaderHelper.convertTextToList(Constants.ASSETS_DICTIONARIES_FOLDER, Word.class);
                    long startTime = System.currentTimeMillis();
                    realm.beginTransaction();
                    for (Word word : words) {
                        realm.copyToRealm(word);
                    }
                    realm.commitTransaction();
                    Timber.d("Inserting of " + words.size() + " items finished in: " + String.valueOf(System.currentTimeMillis() - startTime) + " ms");
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (listener != null) {
                    listener.onLoadFinished();
                }
            }

        }.execute();
    }

    private boolean isWordContainsRequiredLetters(Word word, Map<Character, Integer> uniqueLettersCountMap, int unknownLettersCount) {
        char[] chars = word.getValue().toCharArray();
        for (Character character : chars) {
            if (uniqueLettersCountMap.containsKey(character)) {
                int count = uniqueLettersCountMap.get(character);
                uniqueLettersCountMap.put(character, count - 1);
            }
        }

        int unknownAsMatchedCount = 0;
        int exactMatchedChars = 0;
        for (Map.Entry<Character, Integer> entry : uniqueLettersCountMap.entrySet()) {
            int count = entry.getValue();
            if (count == 0) {
                exactMatchedChars++;
            } else if (count < 0) {
                unknownAsMatchedCount += -count;
                if (unknownAsMatchedCount <= unknownLettersCount) {
                    exactMatchedChars++;
                }
            }
        }

        boolean wordContainsRequiredLetters =
                exactMatchedChars == uniqueLettersCountMap.size();

        return wordContainsRequiredLetters;
    }

    private List<Word> filterWords(String input, List<Word> list) {
        String inputWithoutUnknowLetters = input.replace(String.valueOf(mUnknownSymbol), "");

        if (inputWithoutUnknowLetters.length() < 2) {
            return list;
        }

        Map<Character, Integer> uniqueLettersCountMap = new HashMap<>();
        char[] chars = inputWithoutUnknowLetters.toCharArray();
        for (Character character : chars) {
            if (!character.equals(mUnknownSymbol)) {
                if (uniqueLettersCountMap.containsKey(character)) {
                    uniqueLettersCountMap.put(character, uniqueLettersCountMap.get(character) + 1);
                } else {
                    uniqueLettersCountMap.put(character, 1);
                }
            }
        }

        List<Word> words = new ArrayList<>(list.size());
        for (Word word : list) {
            if (isWordContainsRequiredLetters(word, new HashMap<>(uniqueLettersCountMap),
                    input.length() - inputWithoutUnknowLetters.length())) {
                words.add(word);
            }
        }
        return words;
    }

    @Override
    public void findWords(final String letters, final OnSearchFinishedListener listener) {
        final Realm realm = Realm.getDefaultInstance();
        RealmQuery<Word> query = realm.where(Word.class);
        char[] includeChars = letters.toCharArray();//chars which result should contains

        for (int j = 0; j < letters.length(); j++) {
            char symbol = includeChars[j];
            if (!mUnknownSymbol.equals(symbol)) {
                query.contains("value", String.valueOf(symbol));
            }
        }

        query.equalTo("size", letters.length());

        if (!letters.contains(String.valueOf(mUnknownSymbol))) {
            char[] allChars = mContext.getString(R.string.alphabet).toCharArray();
            List<Character> list = new ArrayList<>();
            for (char symbol : allChars) {
                boolean finded = false;
                for (char includeSymbol : includeChars) {
                    if (symbol == includeSymbol) {
                        finded = true;
                    }
                }
                if (!finded) {
                    if (!list.contains(symbol)) {
                        list.add(symbol);
                    }
                }
            }
            for (char symbol : list) {
                query.not().contains("value", String.valueOf(symbol));
            }
        }

        final long startTime = System.currentTimeMillis();
        final RealmResults<Word> result = query.findAllAsync();
        result.addChangeListener(new RealmChangeListener() {
            @Override
            public void onChange() {
                if (result.isLoaded()) {
                    List<Word> words = filterWords(letters, result.subList(0, result.size()));
                    listener.onSearchFinished(words);
                    Timber.d("Quering of " + result.size() + " items finished in: " + String.valueOf(System.currentTimeMillis() - startTime) + " ms");
                }
            }
        });
    }
}