package com.vsdeni.anagramator.model.interactors;

import com.vsdeni.anagramator.model.Word;

import java.util.List;

/**
 * Created by Deni on 19.04.2016.
 */
public interface WordsInteractor {
    void loadWords(OnLoadFinishedListener listener);

    void findWords(String letters, OnSearchFinishedListener listener);

    interface OnSearchFinishedListener {
        void onSearchFinished(List<Word> words);
    }

    interface OnLoadFinishedListener {
        void onLoadFinished();
    }
}