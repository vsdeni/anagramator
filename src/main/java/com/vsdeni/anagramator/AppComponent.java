package com.vsdeni.anagramator;

import com.vsdeni.anagramator.ui.activities.MainActivity;

import dagger.Component;

/**
 * Created by Deni on 12-May-16.
 */
@Component(modules = AppModule.class)
public interface AppComponent {
    void inject(MainActivity activity);
}
