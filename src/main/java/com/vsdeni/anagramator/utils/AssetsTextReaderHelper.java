package com.vsdeni.anagramator.utils;

import android.content.res.AssetManager;

import com.vsdeni.anagramator.App;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Deni on 22.04.2016.
 */
public class AssetsTextReaderHelper {
    public static <T> List<T> convertTextToList(String folder, Class t) {
        AssetManager am = App.getInstance().getAssets();
        List<T> items = new ArrayList<>();
        try {
            String[] list = am.list(folder);
            for (String file : list) {
                InputStream is = am.open(folder + "/" + file);
                InputStreamReader inputStreamReader = new InputStreamReader(is);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    T test = (T) Class.forName(t.getName()).getConstructor(String.class).newInstance(line.trim());
                    items.add(test);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return items;
    }
}
